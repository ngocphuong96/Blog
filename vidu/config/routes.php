<?php
/**

 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

use Cake\Core\Plugin;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;

Router::defaultRouteClass(DashedRoute::class);

Router::scope('/', function (RouteBuilder $routes) {
    
   // $routes->connect('/auth', ['controller' => 'Users', 'action' => 'index']);

    //$routes->connect('/login', ['controller' => 'Users', 'action' => 'login']);

   // $routes->connect('/logout',['controller'=>'Users','action'=>'logout']);
    $routes->connect('/',['controller'=>'Pages','action'=>'display','home']);
    //$routes->connect('/',['controller'=>'Articles','action'=>'index']);
    //$routes->connect('/add',['controller'=>'Users','action'=>'add']);
   // $routes->connect('/index',['controller'=>'Users','action'=>'index']);

    /**
     * Connect catchall routes for all controllers.
     *
     * Using the argument `DashedRoute`, the `fallbacks` method is a shortcut for
     *    `$routes->connect('/:controller', ['action' => 'index'], ['routeClass' => 'DashedRoute']);`
     *    `$routes->connect('/:controller/:action/*', [], ['routeClass' => 'DashedRoute']);`
     *
     * Any route class can be used with this method, such as:
     * - DashedRoute
     * - InflectedRoute
     * - Route
     * - Or your own route class
     *
     * You can remove these routes once you've connected the
     * routes you want in your application.
     */
    $routes->fallbacks(DashedRoute::class);
});

/**
 * Load all plugin routes. See the Plugin documentation on
 * how to customize the loading of plugin routes.
 */
Plugin::routes();
