<?php 
namespace App\Model\Table;
use Cake\ORM\Table;
use Cake\Validation\Validator;
class ArticlesTable extends Table{
	public function initialize(array $confing){
		$this->addBehavior('Timestamp');
	}
	public function validationDefault(Validator $validator){
		$validator
			->notEmpty('title')
			->requirePresence('title')
			->notEmpty('body')
			->requirePresence('body');
			return $validator;
	}
	public function isOwnedBy($articlseId,$userId){
		return $this->exists(['id'=>$articlseId,'user_id'=>$userId]);
	}
}
 ?>
