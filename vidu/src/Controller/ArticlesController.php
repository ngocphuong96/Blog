<?php
namespace App\Controller;
class ArticlesController extends AppController{
	public function initialize(){
		parent::initialize();
		$this->loadComponent('Flash');
	}
	public function index(){
		$articles = $this->Articles->find('all');
		$this->set(compact('articles'));
	}
	public function view($id= null){
		$articles = $this->Articles->get($id);
		$this->set(compact('articles'));
	}
	public function add(){
		$articles= $this->Articles->newEntity();
		if($this->request->is('post')){
			$articles= $this->Articles->patchEntity($articles,$this->request->getdate());
			$articles->user_id= $this->Auth->user('id');
			if($this->Articles->save($articles)){
				$this->Flash->success(__("You articles has been saved"));
				return $this->redirect(['action'=>'index']);
			}
			$this->Flash->error(__('Unable to add your articles.'));
		}
		$this->set('articles',$articles);
		//$categories = $this->Articles->Categories->find('treeList');
		//$this->set(compact('categories'));
	}
	public function edit($id=null){
		$articles = $this->Articles->get($id);
		if($this->request->is(['post','put'])){
			$this->Articles->patchEntity($articles, $this->request->getData());
			if($this->Articles->save($articles)){
				$this->Flash->success(__('Your articles has been updated.'));
				return $this->redirect(['action'=>'index']);
			}
			$this->Flash->error(__('Unable to update your article'));
		}
		$this->set('articles',$articles);
	}
	public function delete($id){
		$this->request->allowMethod(['post','delete']);
		$articles = $this->Articles->get($id);
		if($this->Articles->delete($articles)){
			$this->Flash->success(__('The articles with id: {0} has been deleted.',h($id)));
				return $this->redirect(['action'=>'index']);
		}
	}
	public function isAuthorized($user){
		if($this->request->getParam('action')==='add'){
			return true;
		}
		if(in_array($this->request->getParam('action'), ['edit','delete'])){
			$articleId = (int)$this->request->getParam('pass.0');
			if($this->Articles->isOwnedBy($articleId,user['id'])){
				return true;
			}
		}
		return parent::isAuthorized($user);
	}
}
?>