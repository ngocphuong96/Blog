<h1>Blog articles</h1>
<?= $this->Html->link('Add Articles',['action'=>'add']) ?>
<table>
	<tr>
		<th>Id</th>
		<th>Title</th>
		<th>Created</th>
		<th>Delete</th>
		<th>Edit</th>
	</tr>

	<?php foreach ($articles as $articles): ?>
	<tr>
		<td><?= $articles->id ?></td>
		<td><?= $this->Html->link($articles->title,['action'=>'view',$articles->id]) ?></td>
		<td><?= $articles->created->format(DATE_RFC850) ?></td>
		<td>
			<?= $this->Form->postlink('Delete',['action'=>'delete',$articles->id],
												['confirm'=>"are you sure"]) ?>
		</td>
		<td><?= $this->Html->link('Edit',['action'=>'edit',$articles->id]) ?></td>
	</tr>
		<?php endforeach; ?>
	
	</table>
