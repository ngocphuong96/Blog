<h1>Edit Articles</h1>
<?php
	echo $this->Form->create($articles);
	echo $this->Form->control('title');
	echo $this->Form->control('body',['rows'=>'3']);
	echo $this->Form->button(__('Save Article'));
	echo $this->Form->end();
?>